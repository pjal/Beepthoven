/*
Beepthoven
2013 Philippe Jaleev
This code is made available under a GPLv3 License -- http://opensource.org/licenses/GPL-3.0
*/

package src.beepthoven;

import java.awt.BorderLayout;
import java.awt.FileDialog;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class Application {
	static String notes = "";
	static JFrame winReader = new JFrame();
	static boolean readInit = false;
	static final JTextArea text = new JTextArea(notes);
	public static void main(String[] args) {
		JFrame window = new JFrame();
		window.setLayout(new BorderLayout());
		JPanel upper = new JPanel();
		upper.setLayout(new GridLayout(1,21));
		JPanel lower = new JPanel();
		lower.setLayout(new GridLayout(1,21));
		JPanel controls = new JPanel();
		controls.setLayout(new FlowLayout());
		JPanel reader = new JPanel();
		reader.add(new JTextField("test"));
		JButton[][] keys = new JButton[21][2];
		for(int j=0;j<42;j++){
			final int i = j;
			if(j<21){
				if(j%7!=0 && j%7!=3){
					keys[j][0] = new JButton(getKey(j));
					keys[j][0].addActionListener(new ActionListener(){
			        	public void actionPerformed(ActionEvent evt){
			        		printNote(i);
			        		text.setText(notes);
			        	}
					});
					upper.add(keys[j][0]);
				}else{
					upper.add(new JPanel());
				}
			}else{
				keys[j-21][1] = new JButton(getKey(j));
				keys[j-21][1].addActionListener(new ActionListener(){
		        	public void actionPerformed(ActionEvent evt){
		        		printNote(i);
		        		text.setText(notes);
		        	}
				});
				lower.add(keys[j-21][1]);	
			}
		}
		JButton exit = new JButton("Exit");
		exit.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		System.exit(0);
        	}
		});
		JButton btnReader = new JButton("Reader");
		
		btnReader.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		if(readInit)
        			winReader.setVisible(true);
        		else
        			read();
        	}
		});
		controls.add(exit);
		controls.add(btnReader);
		window.add(upper, BorderLayout.NORTH);
		window.add(lower,BorderLayout.CENTER);
		window.add(controls,BorderLayout.SOUTH);
		window.setSize(1300,300);
		window.setResizable(false);
		window.setTitle("Beepthoven");
		window.setVisible(true);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static String getKey(int i){
		if(i<21){
			switch(i%7){
				case 0:return "B#";
				case 1:return "C#";
				case 2:return "D#";
				case 3:return "E#";
				case 4:return "F#";
				case 5:return "G#";
				case 6:return "A#";
			}
		}else{
			switch(i%7){
				case 0:return "C";
				case 1:return "D";
				case 2:return "E";
				case 3:return "F";
				case 4:return "G";
				case 5:return "A";
				case 6:return "B";
			}
		}
		return "error";
	}
	
	public static void printNote(int i){
		String freq = "";
		switch(i+1){
		case 2:freq="138.59";break;
		case 3:freq="155.56";break;
		case 5:freq="185.00";break;
		case 6:freq="207.65";break;
		case 7:freq="233.08";break;
		case 9:freq="277.18";break;
		case 10:freq="311.13";break;
		case 12:freq="369.99";break;
		case 13:freq="415.30";break;
		case 14:freq="466.16";break;
		case 16:freq="554.37";break;
		case 17:freq="622.25";break;
		case 19:freq="739.99";break;
		case 20:freq="830.61";break;
		case 21:freq="932.33";break;
		case 22:freq="130.81";break;
		case 23:freq="146.83";break;
		case 24:freq="164.81";break;
		case 25:freq="174.61";break;
		case 26:freq="196.00";break;
		case 27:freq="220.00";break;
		case 28:freq="246.94";break;
		case 29:freq="261.63";break;
		case 30:freq="293.66";break;
		case 31:freq="329.63";break;
		case 32:freq="349.23";break;
		case 33:freq="392.00";break;
		case 34:freq="440.00";break;
		case 35:freq="493.88";break;
		case 36:freq="523.25";break;
		case 37:freq="587.33";break;
		case 38:freq="659.26";break;
		case 39:freq="698.46";break;
		case 40:freq="783.99";break;
		case 41:freq="880.00";break;
		case 42:freq="987.77";break;
		}
		try {
			String command = "beep -f " + freq;
			Process perkele = Runtime.getRuntime().exec(command);
			} catch (IOException e) {
		}
		notes += "beep -f " + freq + " -l 200\n";
	}
	
	public static void play(){
		try {
			Scanner scanner = new Scanner(notes);
			while (scanner.hasNextLine()) {
			  String line = scanner.nextLine();
			  if(!line.isEmpty()){
				  if(line.startsWith("beep ")){
					  Process perkele = Runtime.getRuntime().exec(line);
					  perkele.waitFor();}
				  else{
					  JOptionPane.showMessageDialog(new JFrame(), "Make sure the reader only contains beeps.");
				  }
			  }
			}
			scanner.close();
		} catch (IOException | InterruptedException e) {JOptionPane.showMessageDialog(new JFrame(), "Console exclaimed: " + e +"\nMake sure the reader only contains beeps.");}
	}
	
	public static void read(){
		
		
		readInit=true;
		
		winReader.setVisible(true);
		winReader.setSize(300,600);
		winReader.setResizable(false);
		winReader.setTitle("Beepthoven Reader");
		winReader.setLayout(new BorderLayout());
		
		JButton save = new JButton("Save");
		save.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		FileDialog fDialog = new FileDialog(new JFrame(), "Save", FileDialog.SAVE);
                fDialog.setVisible(true);
        		try {
        		FileWriter recorder = new FileWriter(new File(fDialog.getDirectory() + fDialog.getFile()+".sh"));
					recorder.write(notes);
					recorder.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
        	}
		});
		JButton load = new JButton("Load");
		load.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		FileDialog fDialog = new FileDialog(new JFrame(), "Load", FileDialog.LOAD);
                fDialog.setVisible(true);
        		StringBuilder sb = new StringBuilder();
        		if(fDialog.getDirectory()!=null && fDialog.getFile()!=null){
                try {
                    String textLine;
                    BufferedReader br = new BufferedReader(new FileReader(new File(fDialog.getDirectory() + fDialog.getFile())));
                    while ((textLine = br.readLine()) != null) {
                        sb.append(textLine);
                        sb.append('\n');
                    }
                }catch(Exception ex) {
                    System.out.println(ex.getMessage());
                }finally {
                    if (sb.length() ==  0)
                        sb.append("\n");
                }
               	notes = sb.toString();
               	text.setText(notes);}
        	}
		});
		JButton play = new JButton("Play");
		play.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		play();
        	}
		});
		text.getDocument().addDocumentListener(new DocumentListener() {

	        public void removeUpdate(DocumentEvent e) {
	        	notes = text.getText();
	        }
	        
	        public void insertUpdate(DocumentEvent e) {
	        	notes = text.getText();
	        }

			public void changedUpdate(DocumentEvent e){}     
			
		});
		
		winReader.add(new JScrollPane(text), BorderLayout.CENTER);
		JPanel controls = new JPanel();
		controls.add(play);
		controls.add(save);
		controls.add(load);
		winReader.add(controls, BorderLayout.SOUTH);
	}
}
