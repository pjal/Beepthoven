Beepthoven
==========

**DEPRECATED**  
Due to technical challenges, lack of cross-platform support, and student complaints, Beepthoven is now deprecated in favor of Javkovsky (https://github.com/Kwout/Javkovsky). Javkovsky still saves to .sh, and its reader can execute music as a bash script on linux platforms, for all your beeping needs.

A basic beep music composer.  
Uses the beep package found on linux distros.  
Can load and save music as .sh scripts.  
Programmed in Java.  

2013 © Philippe Jaleev  
Code available under GPLv3 License -- see LICENSE file or visit http://opensource.org/licenses/GPL-3.0
